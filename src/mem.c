#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

extern inline bool region_is_invalid(const struct region *r);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }


static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    if(addr == NULL) return REGION_INVALID;
    bool regions_are_next = true;

    size_t needed_size = size_from_capacity((block_capacity) {.bytes = query}).bytes;
    // making sure size is at least minimal
    const size_t calced_region_size = region_actual_size(needed_size);

    // trying to map pages to memory, and telling function that if the desired place is occupied then throw an error
    void * presumed_reg_addr = map_pages(addr, calced_region_size, MAP_FIXED_NOREPLACE);
    if (MAP_FAILED == presumed_reg_addr) {
        // this time not setting the flag indicating the desired starting position
        presumed_reg_addr = map_pages(addr, calced_region_size, 0);
        if (presumed_reg_addr == MAP_FAILED) return REGION_INVALID;
        regions_are_next = !regions_are_next;
    }

    block_init(presumed_reg_addr, (block_size) {.bytes = calced_region_size}, NULL);
    struct region new_region = (struct region) {
            .addr = presumed_reg_addr,
            .extends = regions_are_next,
            .size = calced_region_size
    };
    return new_region;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if(block == NULL) return false;
    if (!block_splittable(block, query)) return false;

    query = size_max(query, BLOCK_MIN_CAPACITY);

    block_size required_block_s = size_from_capacity((block_capacity) {query});
    block_size block_after_s = (block_size) {size_from_capacity(block->capacity).bytes - required_block_s.bytes};
    block->capacity.bytes -= block_after_s.bytes;

    block_init((void *) block + size_from_capacity(block->capacity).bytes, block_after_s, block->next);
    block->next = (void *) block + size_from_capacity(block->capacity).bytes;

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (block == NULL) return false;

    const struct block_header *next = block->next;
    if (next == NULL || !mergeable(block, next)) return false;

    block_size next_block_s = size_from_capacity(next->capacity);
    block_size ultimate_size = size_from_capacity((block_capacity) {block->capacity.bytes + next_block_s.bytes});
    block_init(block, ultimate_size, next->next);
    return true;
}

/*  --- ... ecли размера кучи хватает --- */
struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    if (block == NULL) return (struct block_search_result) {.type = BSR_CORRUPTED};
    struct block_search_result result = (struct block_search_result) {};
    struct block_header* last = block;
    for (; block; ) {
        while (try_merge_with_next(block));
        if (block->is_free && block_is_big_enough(sz, block)) {
            result.type = BSR_FOUND_GOOD_BLOCK;
            result.block = block;
            return result;
        }
        last = block;
        block = block->next;
    }
    result.type = BSR_REACHED_END_NOT_FOUND;
    result.block = last;
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result find_result = find_good_or_last(block, query);
    if (find_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(find_result.block, query);
        find_result.block->is_free = false;
        return find_result;
    }
    return find_result;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (!last) return NULL;
    void* heap_starts_at = block_after(last);
    struct region allocated_region = alloc_region(heap_starts_at, query);
    if (region_is_invalid(&allocated_region)) return NULL;
    last->next = allocated_region.addr;

    if (allocated_region.extends && try_merge_with_next(last)) return last;
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    if (heap_start == NULL) return NULL;

    // trying to allocate mem in existing heap
    struct block_search_result allocation_state = try_memalloc_existing(query, heap_start);

    if (BSR_FOUND_GOOD_BLOCK == allocation_state.type) return allocation_state.block;
    if (BSR_CORRUPTED == allocation_state.type) return NULL;

    // there is no place in the existing heap, so try to allocate new heap if possible next to the present one
    struct block_header *created_header = grow_heap(allocation_state.block, query);

    // created_header is pointing to blocks of new heap either from old heap or from new one(in this case, it was not possible to allocate new heap right after the previous)
    return created_header ? try_memalloc_existing(query, created_header).block : NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header *block_pointer = (struct block_header *) HEAP_START;
    while (block_pointer != NULL) {
        struct block_header *next_block_pointer = block_pointer->next;
        block_size toFree = size_from_capacity(block_pointer->capacity);
        for (; next_block_pointer && blocks_continuous(block_pointer, next_block_pointer); next_block_pointer = next_block_pointer->next) {
            toFree.bytes += size_from_capacity(next_block_pointer->capacity).bytes;
        }

        munmap(block_pointer, toFree.bytes);
        block_pointer = next_block_pointer;
    }
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
