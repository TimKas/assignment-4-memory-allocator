#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define DEFAULT_HEAP_SIZE 4096
#define HEAP_START ((void*)0x04040000)

void debug(const char* fmt, ... );

#define get_header(mem) \
    ((struct block_header*) (((uint8_t*) (mem)) - offsetof(struct block_header, contents)))

void* map_pages(void const* addr, size_t length, int additional_flags) {
	return mmap((void*) addr,  length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0);
}
void assertGently(bool toBeAsserted, char* test_name, char* err_message, FILE* f){
    if(!toBeAsserted){
        fprintf(f, "%s\n", test_name);
        fprintf(f, "%s", err_message);
    }
}

void testing_malloc(FILE* file){
    debug("TEST 1: Running mem alloc tests\n");
    char* method_name = "testing_malloc:";

    debug("\nInitializing a heap...\n", DEFAULT_HEAP_SIZE);
    void* heap = heap_init(DEFAULT_HEAP_SIZE);

    assertGently(heap, method_name, " heap failed to initialize :)", file);

    debug("Current amount of bytes in new heap: %d\n", DEFAULT_HEAP_SIZE);

    debug("\n- ----- allocating bytes ----- \n");

    debug("trying to allocate %d bytes\n", 1024);
    void* allocated = _malloc(1024);
    assertGently(allocated, method_name, "1024 allocation failed", file);

    debug("allocation succeeded\n");

    debug("\ntrying to allocate a lot of bytes\n");
    debug("allocating %d bytes\n", 9343);
    allocated = _malloc(9343);
    assertGently(allocated, method_name, "9343 allocation failed", file);
    debug("allocation succeeded\n");


    heap_term();
    debug("\n------- test 1 - OK -------\n");
}

void testing_freeing_block(FILE* file) {
    char* method_name = "testing_freeing_block:";
    debug("\n\nTEST 2: Running free func...\n");

    debug("\nInitializing a heap...\n", 0);

    void* heap = heap_init(0);
    assertGently(heap, method_name, "heap did not initialize :)", file);
    debug("Heap was initialized with %d bytes\n", 0);


    debug("\nBlocks initialization...\n", 0);

    void* f_b = _malloc(15);
    void* s_b = _malloc(26);
    void* t_b = _malloc(40);

    assertGently(f_b, method_name, "15 allocation failed", file);
    assertGently(s_b, method_name, "26 allocation failed", file);
    assertGently(t_b, method_name, "40 allocation failed", file);

    debug("\nSuccessfully initialized three blocks\n", 0);
    debug("\n trying to free first block\n");

    _free(f_b);

    bool toBeAsserted = get_header(f_b)->is_free && !get_header(s_b)->is_free && !get_header(t_b)->is_free;
    assertGently(toBeAsserted, method_name, "memory management failure", file);

    debug("Block freeing tests ran successfully");

    _free(s_b);
    _free(t_b);
    heap_term();
    debug("\n------- test 2 - OK -------\n");
}

void tesing_region_expantion(FILE* file) {
    char* method_name = "tesing_region_expantion:";
    
    debug("\n\nTEST 3: Running expanding funcs...\n");

    debug("\nInitializing a heap...\n", 0);
    struct region* heap = heap_init(0);

    assertGently(heap, method_name, "heap did not initialize :)", file);
    debug("Heap was initialized with %d bytes\n", 0);

    size_t reg_s_before = heap->size;
    debug("\nTrying to overflow\n", 0);
    _malloc(4 * 4096);
    size_t reg_s_after = heap->size;
    debug("\nallocation succeeded\n", 0);

    assertGently(reg_s_before < reg_s_after, method_name, "(reg_s_before < reg_s_after) != true", file);

    heap_term();

    debug("\n------- test 3 - OK -------\n");
}

void tesing_occuped_regions(FILE* file) {
    char* method_name = "tesing_occuped_regions:";

    debug("\n\nTest 5: tesing_occuped_regions...\n");

    debug("\nFilling memory...\n", 0);
    void* allocated_beforehand = map_pages(HEAP_START, 10, MAP_FIXED);
    assertGently(allocated_beforehand, method_name, "heap did not initialize :)", file);

    debug("\nattempting allocation into already filled region...\n", 0);

    void* allocated_new = _malloc(10);
    assertGently(allocated_new, method_name, "memory alloc failure", file);

    assertGently(allocated_beforehand != allocated_new, method_name, "new region is not created", file);

    heap_term();
    debug("\n------- test 4 - OK -------\n");
}


int main() {
    testing_malloc(stderr);
    testing_freeing_block(stderr);
    tesing_region_expantion(stderr);
    tesing_occuped_regions(stderr);
    debug("\n\tAll tests succeeded!\n");
}
